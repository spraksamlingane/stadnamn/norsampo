import React from 'react'
import PropTypes from 'prop-types'
import ObjectListItemLink from './ObjectListItemLink'

const ObjectListItem = props => {
  const { data, makeLink, externalLink, linkAsButton, isFirstValue, collapsedMaxWords, shortenLabel, timeline } = props
  let label = Array.isArray(data.prefLabel) ? data.prefLabel[0] : data.prefLabel
  let timespan = null
  
  // TODO: move sorting and max/min to mapping script
  if (timeline && data.dates) {
    if (Array.isArray(data.dates)) {
      let sorted = data.dates.sort()
      let min = sorted[0]
      let max = sorted[sorted.length-1]
      timespan = `${min}-${max}: `
      
    } else {
      timespan = data.dates + ": "
    }
  }

  if ((isFirstValue || shortenLabel) && collapsedMaxWords) {
    const wordCount = label.split(' ').length
    if (wordCount > collapsedMaxWords) {
      label = label.trim().split(' ').splice(0, props.collapsedMaxWords).join(' ')
    }
  }
  return (
    <>
    {timeline && timespan}
      {!makeLink && label}
      {makeLink &&
        <ObjectListItemLink
          data={data}
          label={label}
          externalLink={externalLink}
          linkAsButton={linkAsButton}
        />}
    </>
  )
}

ObjectListItem.propTypes = {
  data: PropTypes.object.isRequired,
  makeLink: PropTypes.bool.isRequired,
  externalLink: PropTypes.bool.isRequired,
  linkAsButton: PropTypes.bool
}

export default ObjectListItem
