import React from 'react'
import Immutable from 'immutable'
import PropTypes from 'prop-types'
import intl from 'react-intl-universal'
import withStyles from '@mui/styles/withStyles';
import { sourceRenderer, labelRenderer, cadastralRenderer,  municipalityRenderer, countyRenderer, collectionRenderer, datasetRenderer} from '../../helpers/renderers';
import { has } from 'lodash'
import {
  AutoSizer,
  Column,
  Table,
  SortIndicator
} from 'react-virtualized'

// https://github.com/bvaughn/react-virtualized/issues/650
// https://github.com/bvaughn/react-virtualized/blob/master/docs/usingAutoSizer.md

const styles = theme => ({
  root: props => ({
    height: 500,
    [theme.breakpoints.up(props.layoutConfig.hundredPercentHeightBreakPoint)]: {
      height: `calc(100% - ${props.layoutConfig.tabHeight}px)`
    },
    borderTop: '1px solid rgb(224, 224, 224)',
    backgroundColor: theme.palette.background.paper,
    '& a': {
      textDecoration: 'underline'
    }
  }),
  resultsInfo: {
    flexGrow: 0
  }
})

const tableStyles = {
  tableRoot: {
    fontFamily: 'Roboto'
  },
  headerRow: {
    textTransform: 'none'
    // borderBottom: '1px solid rgba(224, 224, 224, 1)'
  },
  evenRow: {
    borderBottom: '1px solid rgba(224, 224, 224, 1)'
    // backgroundColor: '#fafafa'
  },
  oddRow: {
    borderBottom: '1px solid rgba(224, 224, 224, 1)'
  },
  noRows: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '1em',
    color: '#fdf4f5'
  }
}

const columnWidth = 115

const calculateRowStyle = ({ index }) => {
  if (index < 0) {
    return tableStyles.headerRow
  } else {
    return index % 2 === 0 ? tableStyles.evenRow : tableStyles.oddRow
  }
}

/**
 * A component for displaying large facet result sets as a virtualized table, without pagination.
 * Based on React Virtualized.
 */
class VirtualizedTable extends React.PureComponent {
  constructor (props) {
    super(props)
    this._noRowsRenderer = this._noRowsRenderer.bind(this)
    this._sort = this._sort.bind(this)
  }

  render () {
    const { classes, list, perspectiveID } = this.props
    // console.log(list)
    const rowGetter = ({ index }) => this._getDatum(list, index)

    const headerRenderer = ({
      dataKey,
      label,
      sortBy,
      sortDirection
    }) => {
      const showSortIndicator = sortBy === dataKey
      const children = [
        <span
          className='ReactVirtualized__Table__headerTruncatedText'
          style={showSortIndicator ? {} : { marginRight: 16 }}
          key='label'
          title={label}
        >
          {label}
        </span>
      ]
      if (showSortIndicator) {
        children.push(
          <SortIndicator key='SortIndicator' sortDirection={sortDirection} />
        )
      }
      return children
    }
  


    return (
      <div className={classes.root}>
        {this.props.list.size > 0 &&
          <AutoSizer>
            {({ height, width }) => (
              <Table
                overscanRowCount={10}
                rowHeight={64}
                rowGetter={rowGetter}
                rowCount={this.props.list.size}
                sort={this._sort}
                sortBy={this.props.clientFSState.sortBy}
                sortDirection={this.props.clientFSState.sortDirection.toUpperCase()}
                width={width}
                height={height}
                headerHeight={50}
                noRowsRenderer={this._noRowsRenderer}
                style={tableStyles.tableRoot}
                rowStyle={calculateRowStyle}
              >
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.prefLabel.label`)}
                  cellDataGetter={({ rowData }) => rowData }
                  dataKey='prefLabel'
                  headerRenderer={headerRenderer}
                  cellRenderer={labelRenderer}
                  width={columnWidth + 120}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.source.label`)}
                  cellDataGetter={({rowData}) => rowData}
                  dataKey='keyword'
                  headerRenderer={headerRenderer}
                  cellRenderer={sourceRenderer}
                  width={columnWidth}
                />

                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.collectionYear.label`)}
                  cellDataGetter={({rowData}) => rowData.collectionYear}
                  dataKey='datasetCode'
                  headerRenderer={headerRenderer}
                  cellRenderer={({cellData}) => (<div>{cellData}</div>) }
                  width={columnWidth}
                />


                                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.cadastralNumbers.label`)}
                  cellDataGetter={({rowData}) => rowData }
                  dataKey="cadastralKey"
                  headerRenderer={headerRenderer}
                  cellRenderer={cadastralRenderer}
                  width={columnWidth}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.municipalityLabel.label`)}
                  cellDataGetter={({rowData}) => ({uri: rowData.municipality, 
                                                  label: rowData.municipalityLabel})}
                  dataKey='municipalityLabel'
                  headerRenderer={headerRenderer}
                  cellRenderer={municipalityRenderer}
                  width={columnWidth}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.countyLabel.label`)}
                  cellDataGetter={({rowData}) => ({uri: rowData.county, 
                                                  label: rowData.countyLabel})}
                  dataKey='countyLabel'
                  headerRenderer={headerRenderer}
                  cellRenderer={countyRenderer}
                  width={columnWidth}
                />

                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.collection.label`)}
                  cellDataGetter={({rowData}) => rowData}
                  dataKey='collectionLabel'
                  headerRenderer={headerRenderer}
                  cellRenderer={collectionRenderer}
                  width={columnWidth}
                />

                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.datasetCode.label`)}
                  cellDataGetter={({rowData}) => ({uri: rowData.dataset, 
                    label: rowData.datasetCode})}
                  dataKey='datasetCode'
                  headerRenderer={headerRenderer}
                  cellRenderer={datasetRenderer}
                  width={columnWidth}
                />
              </Table>
            )}
          </AutoSizer>}
      </div>
    )
  }

  _getDatum (list, index) {
    return list.get(index % list.size)
  }

  _getRowHeight ({ index }) {
    const list = this.props.list
    return this._getDatum(list, index).size
  }

  _noRowsRenderer () {
    return <div className={tableStyles.noRows}>No rows</div>
  }

  // _onScrollToRowChange(event) {
  //   const {rowCount} = this.state;
  //   let scrollToIndex = Math.min(
  //     rowCount - 1,
  //     parseInt(event.target.value, 10),
  //   );
  //
  //   if (isNaN(scrollToIndex)) {
  //     scrollToIndex = undefined;
  //   }
  //
  //   this.setState({scrollToIndex});
  // }

  // https://stackoverflow.com/questions/40412114/how-to-do-proper-column-filtering-with-react-virtualized-advice-needed
  _sort ({ sortBy, event, sortDirection }) {
    if (has(event.target, 'className') && event.target.className.startsWith('Mui')) {
      event.stopPropagation()
    } else {
      this.props.clientFSSortResults({ sortBy, sortDirection: sortDirection.toLowerCase() })
    }
  }
}

VirtualizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  list: PropTypes.instanceOf(Immutable.List).isRequired,
  clientFSState: PropTypes.object,
  clientFSSortResults: PropTypes.func,
  perspectiveID: PropTypes.string.isRequired
}

export const VirtualizedTableComponent = VirtualizedTable

export default withStyles(styles)(VirtualizedTable)
