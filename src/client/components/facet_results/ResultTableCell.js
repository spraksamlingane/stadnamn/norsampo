import React from 'react'
import PropTypes from 'prop-types'
import TableCell from '@mui/material/TableCell'
import ObjectListCollapsible from './ObjectListCollapsible'
import StringList from './StringList'
import SimpleReactLightbox from 'simple-react-lightbox'
import AudioButton from '../../components/custom/AudioButton'
import ImageGallerySRL from '../main_layout/ImageGallerySRL'
import IconButton from '@mui/material/IconButton'
import DetailsIcon from '@mui/icons-material/ReadMoreOutlined'
import { Link } from 'react-router-dom'
import Tooltip from '@mui/material/Tooltip'
import intl from 'react-intl-universal'

const ResultTableCell = props => {
  const {
    data, tableData, valueType, makeLink, externalLink, sortValues, sortBy, sortByConvertDataTypeTo,
    numberedList, minWidth, height, container, columnId, expanded, linkAsButton, collapsedMaxWords, showSource,
    sourceExternalLink, renderAsHTML, HTMLParserTask, referencedTerm, previewImageHeight,
    onExpandClick, showExtraCollapseButton, rowId, shortenLabel = false, timeline
  } = props
  let cellContent = null
  const cellStyle = {
    paddingTop: 3,
    paddingBottom: 3,
    ...(height && { height }),
    ...(minWidth && { minWidth })
  }
  switch (valueType) {
    case 'object':
      cellContent = (
        <ObjectListCollapsible
          data={data}
          tableData={tableData}
          makeLink={makeLink}
          externalLink={externalLink}
          sortValues={sortValues}
          sortBy={sortBy}
          sortByConvertDataTypeTo={sortByConvertDataTypeTo}
          numberedList={numberedList}
          rowId={rowId}
          columnId={columnId}
          expanded={expanded}
          onExpandClick={onExpandClick}
          collapsedMaxWords={collapsedMaxWords}
          shortenLabel={shortenLabel}
          linkAsButton={linkAsButton}
          showSource={showSource}
          sourceExternalLink={sourceExternalLink}
          timeline={timeline}
        />
      )
      break
    case 'details':
        cellContent = (
          <div className="problem" style={{display: 'flex', alignItems: 'center'}}>
          <Tooltip disableFocusListener title={intl.get('table.details')}>
          <Link to={data.dataProviderUrl}><IconButton aria-label={intl.get('table.details')}><DetailsIcon aria-hidden={true}/></IconButton></Link>
          </Tooltip>
          {
            data.audioFilename && 
            <AudioButton audioFile={ "https://iiif.test.ubbe.no/iiif/audio/" + data.datasetCode.toLowerCase() + "/"  + data.audioFilename + ".wav" }/>

          }
          
          </div>
        )
        break
    case 'audio':     
        cellContent = data && data !== '-' ? (
          <audio controls src={"https://iiif.test.ubbe.no/iiif/audio/" + data.datasetCode.toLowerCase() + "/"  + data.audioFilename + ".wav"}/>
          //<audio controls src={"https://iiif.test.ubbe.no/iiif/audio/" + data.datasetCode.toLowerCase() + "/"  + data.audioFilename + ".wav"}/>
          
        ) : ''
        break
    case 'string':
      cellContent = (
        <StringList
          data={data}
          tableData={tableData}
          expanded={expanded}
          onExpandClick={onExpandClick}
          rowId={rowId}
          collapsedMaxWords={collapsedMaxWords}
          showExtraCollapseButton={showExtraCollapseButton}
          shortenLabel={shortenLabel}
          renderAsHTML={renderAsHTML}
          HTMLParserTask={HTMLParserTask}
          referencedTerm={referencedTerm}
          numberedList={numberedList}
          timeline={timeline}
        />
      )
      break
    case 'image':
      cellContent = data && data !== '-'
        ? <SimpleReactLightbox><ImageGallerySRL data={data} previewImageHeight={previewImageHeight} /></SimpleReactLightbox>
        : ''
  }
  if (container === 'div') {
    return (
      <div>
        {cellContent}
      </div>
    )
  }
  if (container === 'cell') {
    return (
      <TableCell style={cellStyle}>
        {cellContent}
      </TableCell>
    )
  }
}

ResultTableCell.propTypes = {
  columnId: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  valueType: PropTypes.string.isRequired,
  makeLink: PropTypes.bool.isRequired,
  externalLink: PropTypes.bool.isRequired,
  sortValues: PropTypes.bool.isRequired,
  sortBy: PropTypes.string,
  numberedList: PropTypes.bool.isRequired,
  expanded: PropTypes.bool.isRequired,
  collapsedMaxWords: PropTypes.number,
  minWidth: PropTypes.number,
  maxWidth: PropTypes.number,
  previewImageHeight: PropTypes.number,
  showSource: PropTypes.bool,
  sourceExternalLink: PropTypes.bool
}

export default ResultTableCell
