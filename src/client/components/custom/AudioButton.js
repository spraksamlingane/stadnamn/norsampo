import React, { Component } from 'react'
import PlayCircleIcon from '@mui/icons-material/PlayCircle'
import StopCircleIcon from '@mui/icons-material/StopCircle'
import IconButton from '@mui/material/IconButton'
class AudioButton extends Component {

constructor(props) {
  super(props)
  this.state = {
    playing: false
  }
  this.audio = null
}


togglePlay = () => {
    if (!this.audio) {
      this.audio = new Audio(this.props.audioFile)
      this.audio.play()
      this.setState({ playing: true });
      this.audio.addEventListener('ended', () => {
        this.setState({ playing: false });
        this.audio = null;
      });
    }
    else {
      this.setState({ playing: false });
      this.audio.pause();
      this.audio = null;
    }    
  }

  render() {
      return (
         <IconButton onClick={this.togglePlay}>{this.state.playing ? <StopCircleIcon/> : <PlayCircleIcon/>}</IconButton>
      )
  }
}

export default AudioButton