import React from 'react'
import IconButton from '@mui/material/IconButton'
import PlaceIcon from '@mui/icons-material/Place'
import ImageIcon from '@mui/icons-material/Image'
import SourceIcon from '@mui/icons-material/Source'
import AudioButton from '../components/custom/AudioButton'
import AudioFileIcon from '@mui/icons-material/AudioFile'
import parse from 'html-react-parser';
import DOMPurify from 'dompurify';


export const popupSourceRenderer = ({cellData}) => {
    let {id, imageManifest, rawDataURI, soundManifest, audioFileID, datasetCode, location} = cellData
        return (
        <div key={id}>
         { (typeof location !== 'undefined') &&
        <a href="https://lenke_til_landing_page_for_koordinater">
            <span><PlaceIcon /> Koordinater </span>
            </a>
         }
        {imageManifest && 
            <a target='_blank' rel='noopener noreferrer' href={'https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/'+ imageManifest + ".json"}>
            <IconButton aria-label='Seddel' size="large"> Seddel
        <ImageIcon />
        </IconButton>
        </a>
        }
        {rawDataURI &&
            <a target='_blank' rel='noopener noreferrer' href={rawDataURI}>
                <IconButton aria-label='Kildedata' size="large"> Kildedata
            <SourceIcon />
            </IconButton>
            </a>
        }
        { soundManifest &&
            <a target='_blank' rel='noopener noreferrer' href={'https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/'+ soundManifest + ".json"}>
            <AudioFileIcon /> Informasjon om lydopptak
            </a>
        }
    
        {audioFileID &&
          <div>
          <audio controls src={"https://iiif.test.ubbe.no/iiif/audio/" + datasetCode.toLowerCase() + "/"  + audioFileID + ".wav"}/>
          </div>
        }
        </div>
        )
  }

export const sourceRenderer = ({cellData}) => {
    let {id, imageManifest, rawDataURI, soundManifest, audioFileID, datasetCode, location } = cellData
        return (
        <div key={id}>
         {
        (typeof location !== 'undefined') &&
        <a href="https://lenke_til_landing_page_for_koordinater">
             
            <IconButton aria-label='Koordinater' size="large">
            <PlaceIcon />
            </IconButton> 
            </a>
         }   
        {imageManifest && (
            <a target='_blank' rel='noopener noreferrer' href={'https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/'+ imageManifest + ".json"}>
            <IconButton aria-label='Seddel' size="large">
        <ImageIcon />
        </IconButton>
        </a>
        ) }
        {rawDataURI &&
        (
            <a target='_blank' rel='noopener noreferrer' href={rawDataURI}>
                <IconButton aria-label='Kildedata' size="large">
            <SourceIcon />
            </IconButton>
            </a>
        )
        }
        {soundManifest && 
                <a target='_blank' rel='noopener noreferrer' href={'https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/'+ soundManifest + ".json"}>
                <IconButton aria-label='Kildedata' size="large">
              <AudioFileIcon />
            </IconButton>
              </a>
        }
        {audioFileID &&
            <AudioButton audioFile={ "https://iiif.test.ubbe.no/iiif/audio/" + datasetCode.toLowerCase() + "/"  + audioFileID + ".wav" }/>
        }
        </div>
        )
  }



export const labelRenderer = ({ cellData }, popup) => {
    let { prefLabel, id, placeInfoPath, concatLabels} = cellData
    if (prefLabel == null) return ''
    
    let sublabels = ""
    if (placeInfoPath ){
      prefLabel = ( <a target='_blank' rel='noopener noreferrer' href={placeInfoPath + "/" + id.slice(-36)}>{prefLabel}</a> )
    }

    if (concatLabels) {
      prefLabel = prefLabel.replace(/^\"([^"]*)\"$/, "$1")
      // remoe quotations if they enclose the whole string
      let taggedLabels = concatLabels.split("|").map((item) => DOMPurify.sanitize(
        item.replace(/^\"([^"]*)\"$/, "$1"), 
        {ALLOWED_TAGS: ['b']}).replaceAll('<b>', '<strong>').replaceAll("</b>", "</strong>"))
      taggedLabels = taggedLabels.filter((item, index, arr) => arr.indexOf(item) === index)
      // Clean results for comparison with label
      const cleanLabels = taggedLabels.map(item => item.replace(/(\<\/?strong\>)/g, "")) 
      
      let labelpos = cleanLabels.indexOf(prefLabel)
      if (labelpos == -1) {
        prefLabel = <em>{prefLabel}</em>
      }
      sublabels = (<div>{taggedLabels.map((item, index) => {
        
        let parsed = parse(item)
        if (index == labelpos) {
          prefLabel = parsed
        }
        else {
          return <span key={index}>{index == 0 || index == labelpos + 1 || ' ... '}{parsed}</span>
        }
        
      })}
      </div>)

    }

    if (popup) {
        return (
            <div key={id}>
                <h3>{prefLabel}</h3><p>{sublabels}</p>
            </div>
            )
    }
    else {
        return (
            <div key={id}>
                {prefLabel}{sublabels}
            </div>
            )

    }
    
    
  }


  export const cadastralRenderer = ({ cellData }) => {
    let {id, cadastralUnit, propertyUnit, municipalNumber } = cellData
    // TODO: only show coordinate symbol if connected to historical cadastre (GNIDU/MIDU)

     return ( <div key={id}>
        { (cadastralUnit || propertyUnit) && <IconButton aria-label="Historisk matrikkel"><PlaceIcon/></IconButton> }
       {cadastralUnit || '-'}  { propertyUnit ? " / " + propertyUnit : ""} <br/>{municipalNumber}
     </div> )
   
 }

 

 export const municipalityRenderer = ({ cellData, rowData }) => {
  let { uri, label, infoPath} = cellData
 
 
 if (label) {
   if (uri) {
     return ( <div key={rowData.id}>
     <a target='_blank' rel='noopener noreferrer' href={uri.replace("http://data.spraksamlingane.no/stadnamn/archive/id/", "/municipalities/page/")}>{label}</a>
   </div> )

   }
   else {
     return ( <div key={rowData.id}>
       {label}
     </div> )
   }
 }
 
}

export const countyRenderer = ({ cellData, rowData }) => {
   let { uri, label } = cellData
  if (label) {
    if (uri) {
      return ( <div key={rowData.id}>
      <a target='_blank' rel='noopener noreferrer' href={uri.replace("http://data.spraksamlingane.no/stadnamn/archive/id/", "/counties/page/")}>{label}</a>
    </div> )

    }
    else {
      return ( <div key={rowData.id}>
        {label}
      </div> )
    }
  }
  
}

export const collectionRenderer = ({ cellData, rowData }) => {
  let { collectionID, collection } = cellData

  return ( <div>

    {
      collectionID && (
        <a target='_blank' rel='noopener noreferrer' href={'https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/' + collectionID + '.json'}>{collection}</a>
      )
      || collection
    }

  </div>) 
 
}


export const datasetRenderer = ({ cellData, rowData }) => {
  let { uri, label } = cellData
 
 
 if (label) {
   if (uri) {
     return ( <div key={rowData.id}>
     <a target='_blank' rel='noopener noreferrer' href={uri.replace("http://data.spraksamlingane.no/stadnamn/archive/", "/datasets/page/")}>{label}</a>
   </div> )

   }
   else {
     return ( <div key={rowData.id}>
       {label}
     </div> )
   }
 }
 
}
