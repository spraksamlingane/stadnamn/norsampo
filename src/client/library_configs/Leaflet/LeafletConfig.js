import { has, orderBy } from 'lodash'
import history from '../../History'
import intl from 'react-intl-universal'
import moment from 'moment'
import { popupSourceRenderer, labelRenderer } from '../../helpers/renderers'
import { renderToString } from 'react-dom/server'

export const createPopUpContentDefault = ({ data, resultClass }) => {
  if (Array.isArray(data.prefLabel)) {
    data.prefLabel = data.prefLabel[0]
  }
  const container = document.createElement('div')
  const h3 = document.createElement('h3')
  if (has(data.prefLabel, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.prefLabel.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else if (has(data, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else {
    h3.textContent = data.prefLabel.prefLabel
  }
  container.appendChild(h3)
  return container
}

export const createPopUpContentAs = ({ data, resultClass }) => {
  if (Array.isArray(data.prefLabel)) {
    data.prefLabel = data.prefLabel[0]
  }
  const container = document.createElement('div')
  const h3 = document.createElement('h3')
  if (has(data.prefLabel, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.prefLabel.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else {
    h3.textContent = data.prefLabel.prefLabel
  }
  container.appendChild(h3)
  if (resultClass === 'peoplePlaces' || resultClass === 'placesPeople') {
    const p = document.createElement('p')
    p.textContent = 'People:'
    container.appendChild(p)
    container.appendChild(createInstanceListing(data.related))
  }
  return container
}

export const createPopUpContentLetterSampo = ({ data, resultClass }) => {
  if (Array.isArray(data.prefLabel)) {
    data.prefLabel = data.prefLabel[0]
  }
  const container = document.createElement('div')
  const h3 = document.createElement('h3')
  if (has(data.prefLabel, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.prefLabel.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else {
    h3.textContent = data.prefLabel.prefLabel
  }
  container.appendChild(h3)
  if (resultClass === 'placesActors') {
    const p = document.createElement('p')
    p.textContent = 'Actors:'
    container.appendChild(p)
    container.appendChild(createInstanceListing(data.related))
  }
  return container
}

export const createPopUpContentSotasurmat = ({ data, resultClass }) => {
  if (Array.isArray(data.prefLabel)) {
    data.prefLabel = data.prefLabel[0]
  }
  const container = document.createElement('div')
  const h3 = document.createElement('h3')
  if (has(data.prefLabel, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.prefLabel.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else {
    h3.textContent = data.prefLabel.prefLabel
  }
  container.appendChild(h3)

  if (resultClass === 'deathPlaces') {
    const deathsAtElement = document.createElement('p')
    deathsAtElement.textContent = intl.get('perspectives.victims.map.deathsAt')
    container.appendChild(deathsAtElement)
    container.appendChild(createInstanceListing(data.related))
  }

  if (resultClass === 'battlePlaces') {
    const startDateElement = document.createElement('p')
    const startDate = moment(data.startDate)
    startDateElement.textContent = `${intl.get('perspectives.battles.map.startDate')}: ${startDate.format('DD.MM.YYYY')}`
    container.appendChild(startDateElement)
    const endDateElement = document.createElement('p')
    const endDate = moment(data.endDate)
    endDateElement.textContent = `${intl.get('perspectives.battles.map.endDate')}: ${endDate.format('DD.MM.YYYY')}`
    container.appendChild(endDateElement)
    if (has(data, 'greaterPlace.prefLabel')) {
      const municipalityElement = document.createElement('p')
      municipalityElement.textContent = `${intl.get('perspectives.battles.map.municipality')}: ${data.greaterPlace.prefLabel}`
      container.appendChild(municipalityElement)
    }
    if (has(data, 'units')) {
      const unitsElement = document.createElement('p')
      unitsElement.textContent = `${intl.get('perspectives.battles.map.units')}: ${data.units}`
      container.appendChild(unitsElement)
    }
  }
  return container
}

export const createPopUpContentMMM = ({ data, resultClass }) => {
  if (Array.isArray(data.prefLabel)) {
    data.prefLabel = data.prefLabel[0]
  }
  const container = document.createElement('div')
  const h3 = document.createElement('h3')
  if (has(data.prefLabel, 'dataProviderUrl')) {
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(data.prefLabel.dataProviderUrl))
    link.textContent = data.prefLabel.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    h3.appendChild(link)
  } else {
    h3.textContent = data.prefLabel.prefLabel
  }
  container.appendChild(h3)
  if (resultClass === 'placesMsProduced') {
    const p = document.createElement('p')
    p.textContent = 'Manuscripts produced here:'
    container.appendChild(p)
    container.appendChild(createInstanceListing(data.related))
  }
  if (resultClass === 'lastKnownLocations') {
    const p = document.createElement('p')
    p.textContent = 'Last known location of:'
    container.appendChild(p)
    container.appendChild(createInstanceListing(data.related))
  }
  if (resultClass === 'placesActors') {
    const p = document.createElement('p')
    p.textContent = 'Actors:'
    container.appendChild(p)
    container.appendChild(createInstanceListing(data.related))
  }
  return container
}

export const createPopUpContentPlacenames = ({ data }) => {
  let popupTemplate = `<a href=${data.prefLabel.dataProviderUrl} target='_blank'><h3>${data.prefLabel.prefLabel}</h3></a>
  <p><b title='${intl.get(`perspectives.placenames.properties.variant.description`)}'>
    ${intl.get(`perspectives.placenames.properties.variant.label`)}:</b><br>`

  let variants = data.detailedVariant[0] ? data.detailedVariant : [data.detailedVariant]
  variants.forEach(element => {
    let timespan = ""
      if (Array.isArray(element.dates)) {
        let sorted = element.dates.sort()
        let min = sorted[0]
        let max = sorted[sorted.length-1]
        timespan = `${min}-${max}`
        
      } else {
        timespan = element.dates
      }
    
    popupTemplate += `${timespan}: <a href=${element.dataProviderUrl} target='_blank'>${element.prefLabel}</a><br>`
    // legg til kilder som punkter under hver rad
  })

  popupTemplate += "</p>"

  return popupTemplate
}

export const createPopUpContentHordanamn = ({ data, perspectiveID }) => {

  let popUpTemplate = ""
  popUpTemplate += `<a href=${data.infoPage} target='_blank'><h3>${data.normertForm || data.namn}</h3></a>`

  if (data.namn && (!data.normertForm || data.namn != data.normertForm)) {
    popUpTemplate += `
    <p><strong>${intl.get(`perspectives.hordanamn.properties.namn.label`)}:</strong> ${data.namn}</p>`
  }

  if (data.normertForm && data.namn && data.namn != data.normertForm) {
    popUpTemplate += `
    <p><strong>${intl.get(`perspectives.hordanamn.properties.normertForm.label`)}:</strong> ${data.normertForm}</p>`
  }
  if (data.alternativForm) {
    popUpTemplate += `
    <p><strong>${intl.get(`perspectives.hordanamn.properties.alternativForm.label`)}:</strong> ${data.alternativForm}</p>`
  }

  for (const field of ["uttale","kommuneNamn", "fylkesNamn", "gardsNr", "bruksNr", "merknader"]) {
    if (data[field]) {
      popUpTemplate += `
    <p><strong>${intl.get(`perspectives.hordanamn.properties.${field}.label`)}:</strong> ${data[field]}</p>`
    }
  }

  if (data.source) {
    popUpTemplate += `
      <p>${renderToString(popupSourceRenderer({cellData: data}))}</p>`
  }

  return popUpTemplate
}


export const createPopUpContentNameSampo = ({ data, perspectiveID }) => {
  let popUpTemplate = ''
  if (has(data, 'concatLabels')) {
    popUpTemplate += renderToString(labelRenderer({cellData: data}, true))
  }
  else {
    popUpTemplate += `<a href=${data.id} target='_blank'><h3>${data.prefLabel}</h3></a>`

  }
  if (has(data, 'municipalityLabel')) {
    popUpTemplate += `
      <p><strong>${intl.get(`perspectives.${perspectiveID}.properties.municipalityLabel.label`)}:</strong> ${data.municipalityLabel}</p>`
  }
  if (has(data, 'countyLabel')) {
    popUpTemplate += `
      <p><strong>${intl.get(`perspectives.${perspectiveID}.properties.countyLabel.label`)}</strong>: ${data.countyLabel}</p>`
  }
  if (has(data, 'collectionYear')) {
    popUpTemplate += `
      <p><strong>${intl.get(`perspectives.${perspectiveID}.properties.collectionYear.label`)}</strong>: ${data.collectionYear}</p>`
  }
  if (has(data, 'source') || has(data, 'lat')) {
    popUpTemplate += `
      <section><h4>${intl.get(`perspectives.${perspectiveID}.properties.source.label`)}:</h4> ${renderToString(popupSourceRenderer({cellData: data}))}</section>`
  }
  return popUpTemplate
}

export const createPopUpContentInstancePage = ({ data, perspectiveID }) => {
  
  return ""
}

export const createPopUpContentFindSampo = ({ data }) => {
  const container = document.createElement('div')

  if (has(data, 'image')) {
    let { image } = data
    if (Array.isArray(image)) {
      image = image[0]
    }
    const imageElement = document.createElement('img')
    imageElement.className = 'leaflet-popup-content-image'
    imageElement.setAttribute('src', image.url)
    container.appendChild(imageElement)
  }
  const heading = document.createElement('h3')
  const headingLink = document.createElement('a')
  headingLink.style.cssText = 'cursor: pointer; text-decoration: underline'
  headingLink.textContent = data.prefLabel.prefLabel
  headingLink.addEventListener('click', () => history.push(data.dataProviderUrl))
  heading.appendChild(headingLink)
  container.appendChild(heading)
  if (has(data, 'objectType')) {
    container.appendChild(createPopUpElement({
      label: intl.get('perspectives.finds.properties.objectType.label'),
      value: data.objectType.prefLabel
    }))
  }
  if (has(data, 'material')) {
    container.appendChild(createPopUpElement({
      label: intl.get('perspectives.finds.properties.material.label'),
      value: data.material.prefLabel
    }))
  }
  if (has(data, 'period')) {
    let periodLabel = ''
    if (Array.isArray(data.period)) {
      data.period.forEach((p, index) => {
        periodLabel += `${p.prefLabel}`
        if (index !== data.period.length - 1) {
          periodLabel += ', '
        }
      })
    } else {
      periodLabel = data.period.prefLabel
    }
    container.appendChild(createPopUpElement({
      label: intl.get('perspectives.finds.properties.period.label'),
      value: periodLabel
    }))
  }
  if (has(data, 'municipality')) {
    container.appendChild(createPopUpElement({
      label: intl.get('perspectives.finds.properties.municipality.label'),
      value: data.municipality.prefLabel
    }))
  }
  return container
}

const createPopUpElement = ({ label, value }) => {
  const p = document.createElement('p')
  const b = document.createElement('b')
  const span = document.createElement('span')
  b.textContent = (`${label}: `)
  span.textContent = value
  p.appendChild(b)
  p.appendChild(span)
  return p
}

const createInstanceListing = instances => {
  let root
  if (Array.isArray(instances)) {
    root = document.createElement('ul')
    instances = orderBy(instances, 'prefLabel')
    instances.forEach(i => {
      const li = document.createElement('li')
      const link = document.createElement('a')
      link.addEventListener('click', () => history.push(i.dataProviderUrl))
      link.textContent = i.prefLabel
      link.style.cssText = 'cursor: pointer; text-decoration: underline'
      li.appendChild(link)
      root.appendChild(li)
    })
  } else {
    root = document.createElement('p')
    const link = document.createElement('a')
    link.addEventListener('click', () => history.push(instances.dataProviderUrl))
    link.textContent = instances.prefLabel
    link.style.cssText = 'cursor: pointer; text-decoration: underline'
    root.appendChild(link)
  }
  return root
}

const createArchealogicalSitePopUp = data => {
  let html = ''
  const name = data.kohdenimi
    ? `<b>Nimi:</b> ${data.kohdenimi}</p>`
    : ''
  const classification = data.laji ? `<h3>${data.laji.charAt(0).toUpperCase() + data.laji.slice(1)}</b></h3>` : ''
  const municipality = data.kunta ? `<b>Kunta:</b> ${data.kunta}</p>` : ''
  const link = data.mjtunnus
    ? `<a href="https://www.kyppi.fi/to.aspx?id=112.${data.mjtunnus}" target="_blank">Avaa kohde Muinaisjäännösrekisterissä</a></p>`
    : ''
  html += `
    <div>
      ${classification}
      ${name}
      ${municipality}
      ${link}
    </div>
    `
  return html
}

const bufferStyle = feature => {
  if (feature.properties.laji.includes('poistettu kiinteä muinaisjäännös')) {
    return {
      fillOpacity: 0,
      weight: 0,
      interactive: false
    }
  } else {
    return {
      fillOpacity: 0,
      color: '#6E6E6E',
      dashArray: '3, 5',
      interactive: false
    }
  }
}

const createArchealogicalSiteColor = feature => {
  const entry = fhaLegend.find(el => el.key === feature.properties.laji.trim())
  return entry.color
}

export const fhaLegend = [
  { key: 'kiinteä muinaisjäännös', color: '#f00501' },
  { key: 'luonnonmuodostuma', color: '#00cafb' },
  { key: 'löytöpaikka', color: '#ffb202' },
  { key: 'mahdollinen muinaisjäännös', color: '#fc01e2' },
  { key: 'muu kohde', color: '#ffffff' },
  { key: 'muu kulttuuriperintökohde', color: '#b57b3b' },
  { key: 'poistettu kiinteä muinaisjäännös (ei rauhoitettu)', color: '#8b928b' }
]

/*
  FHA WFS services:
    https://kartta.nba.fi/arcgis/rest/services/WFS/MV_KulttuuriymparistoSuojellut/MapServer
    https://kartta.nba.fi/arcgis/rest/services/WFS/MV_Kulttuuriymparisto/MapServer/
  MV_Kulttuuriymparisto service documentation:
    https://www.paikkatietohakemisto.fi/geonetwork/srv/fin/catalog.search#/metadata/83787bc0-5a11-4429-a79d-22b37360a408
    https://www.museovirasto.fi/uploads/Tietotuotemaarittely_kulttuuriymparisto_kaikki.pdf
  */
export const layerConfigs = [ /*
  {
    // id: 'WFS_MV_KulttuuriymparistoSuojellut:Muinaisjaannokset_alue',
    id: 'WFS_MV_Kulttuuriymparisto:Arkeologiset_kohteet_alue',
    type: 'GeoJSON',
    attribution: 'Museovirasto',
    minZoom: 13,
    buffer: {
      distance: 200,
      units: 'metres',
      style: bufferStyle
    },
    createGeoJSONPolygonStyle: feature => {
      return {
        color: createArchealogicalSiteColor(feature),
        cursor: 'pointer'
      }
    },
    createPopup: createArchealogicalSitePopUp
  },
  {
    // id: 'WFS_MV_KulttuuriymparistoSuojellut:Muinaisjaannokset_piste',
    id: 'WFS_MV_Kulttuuriymparisto:Arkeologiset_kohteet_piste',
    type: 'GeoJSON',
    attribution: 'Museovirasto',
    minZoom: 13,
    buffer: {
      distance: 200,
      units: 'metres',
      style: bufferStyle
    },
    createGeoJSONPointStyle: feature => {
      return {
        radius: 8,
        fillColor: createArchealogicalSiteColor(feature),
        color: '#000',
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
      }
    },
    createPopup: createArchealogicalSitePopUp
  },
  {
    id: 'fhaLidar',
    type: 'WMS',
    url: `${process.env.API_URL}/fha-wms`,
    layers: 'NBA:lidar',
    version: '1.3.0',
    attribution: 'Museovirasto',
    minZoom: 13,
    maxZoom: 18
  },
  {
    id: 'karelianMaps',
    type: 'WMTS',
    url: 'https:///mapwarper.onki.fi/mosaics/tile/4/{z}/{x}/{y}.png',
    opacityControl: true,
    attribution: 'Semantic Computing Research Group'
  },
  {
    id: 'senateAtlas',
    type: 'WMTS',
    url: 'https:///mapwarper.onki.fi/mosaics/tile/5/{z}/{x}/{y}.png',
    opacityControl: true,
    attribution: 'Semantic Computing Research Group'
  }
*/]
