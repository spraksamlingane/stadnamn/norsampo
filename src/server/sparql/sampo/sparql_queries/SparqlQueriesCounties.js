export const countyProperties = `
  {
    ?id a bsn:County .
    ?id rdfs:label ?prefLabel__id .
    BIND(?prefLabel__id AS ?prefLabel__prefLabel)
    BIND(?id as ?uri__id)
  
  }
  UNION {
    ?municipality__id bsn:subunitOf ?id ;
                      rdfs:label ?municipality__prefLabel .
    
    BIND(CONCAT("/municipalities/page/", REPLACE(STR(?municipality__id), "^(.*)(/|#)([^#/]*)$", "$3")) AS ?municipality__dataProviderUrl)
  }
  UNION {
    BIND("http://data.spraksamlingane.no/stadnamn/archive/bsn" AS ?dataset__id)
    BIND("BSN" AS ?dataset__prefLabel)
    BIND("/datasets/page/bsn" AS ?dataset__dataProviderUrl)
  }

  


`
