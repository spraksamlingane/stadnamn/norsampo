// TODO: update ecrm uris for inverse properties
export const snidProperties = `
{
  ?id rdfs:label ?prefLabel__prefLabel .
  ?id dct:identifier ?snid__id .
  BIND(CONCAT("/placenames/page/", REPLACE(STR(?id), "^.*\\\\/(.+)", "$1")) AS ?prefLabel__dataProviderUrl)
  BIND(?id as ?uri__id)
  BIND(?id as ?uri__dataProviderUrl)
  BIND(?id as ?uri__prefLabel)
  }
  UNION {
    ?id ecrm:P139_has_alternative_form ?variant__id .
    ?variant__id rdfs:label ?variant__prefLabel .
    BIND(CONCAT("/variants/page/", REPLACE(STR(?variant__id), "^.*\\\\/(.+)", "$1")) AS ?variant__dataProviderUrl)
  }
  UNION {
    ?id ecrm:P67i_is_referred_to_by ?dataset__id .
    ?dataset__id dct:title ?dataset__prefLabel .
    BIND(CONCAT("/datasets/page/", REPLACE(STR(?g), "^.*\\\\/(.+)", "$1")) AS ?dataset__dataProviderUrl)
  
  }
  UNION {
    ?id ecrm:P139_has_alternative_form ?variant__id .
    ?name_event ecrm:P141_assigned ?variant__id .
    ?name_event ecrm:P70i_is_documented_in ?document__id .
    ?document__id ecrm:P129i_is_subject_of ?document__manifest .
  }
UNION { 
    ?id ecrm:P139_has_alternative_form ?variant__id .
    ?place ecrm:P140i_was_attributed_by/ecrm:P141_assigned ?variant__id ;
           (ecrm:P89_falls_within)* ?kommune__id .
    ?kommune__id ecrm:P1_is_identified_by ?knr__id .
    ?kommune__id rdfs:label ?kommune__prefLabel .
    ?knr__id rdfs:label ?knr__prefLabel .
    
    ?kommune__id ecrm:P89_falls_within ?fylke__id .
    ?fylke__id rdfs:label ?fylke__prefLabel .
  } 
  UNION { 
    ?id ecrm:P139_has_alternative_form ?variant__id .
    ?place ecrm:P140i_was_attributed_by/ecrm:P141_assigned ?variant__id ;
           (ecrm:P89_falls_within)* ?adm__id .
    ?adm__id ecrm:P1_is_identified_by ?admNo__id .
    ?adm__id rdfs:label ?adm__prefLabel .
    ?admNo__id rdfs:label ?admNo__prefLabel .
    
  }  

`

export const snidInstanceProperties = `
  {
  ?id rdfs:label ?prefLabel__id .
  BIND (?prefLabel__id as ?prefLabel__prefLabel)
  BIND(CONCAT("/placenames/page/", REPLACE(STR(?id), "^.*\\\\/(.+)", "$1")) AS ?prefLabel__dataProviderUrl)     
  BIND(?id as ?uri__id)
  BIND(?id as ?uri__dataProviderUrl)
  BIND(?id as ?uri__prefLabel)
  }
  UNION {
    ?id ecrm:P139_has_alternative_form ?detailedVariant__id .
    ?detailedVariant__id a vclass:Name_Variant ;
                 rdfs:label ?detailedVariant__prefLabel . 
     BIND(CONCAT("/variants/page/", REPLACE(STR(?detailedVariant__id), "^.*\\\\/(.+)", "$1")) AS ?detailedVariant__dataProviderUrl)         
  }
  UNION {
    ?id ecrm:P139_has_alternative_form ?detailedVariant__id .  
      ?event ecrm:P141_assigned ?detailedVariant__id .
      ?event ecrm:P70i_is_documented_in/dct:date ?detailedVariant__dates .
  }
  UNION {
    ?id ecrm:P139_has_alternative_form ?detailedVariant__id . 
    ?g dct:source/dct:date ?detailedVariant__dates .
    }
  UNION {
    ?id ecrm:P139_has_alternative_form ?detailedVariant__id .
    ?dataset__id skos:notation ?dataset__prefLabel .
    BIND(CONCAT("/datasets/page/", REPLACE(STR(?g), "^.*\\\\/(.+)", "$1")) AS ?dataset__dataProviderUrl)
  }

`

export const snidCentroidQuery = `
SELECT DISTINCT ?id ?lat ?long
WHERE  {
  <FILTER>
    ?id a vclass:SNID_Name .
    ?id ecrm:P87_is_identified_by ?point .
    ?point wgs84:lat ?lat ;
      wgs84:long ?long .
  
}
`

export const snidPlaceMapQuery = `
  SELECT * WHERE { 
    VALUES ?inputID { <ID> }
    <FILTER>
    
      ?inputID ecrm:P139_has_alternative_form ?variant__id .
      ?id__id ecrm:P140i_was_attributed_by/ecrm:P141_assigned ?variant__id;
                 ecrm:P87_is_identified_by ?coordinates .
      ?coordinates wgs84:lat ?lat;
                   wgs84:long ?long .
      ?variant__id rdfs:label ?variant__prefLabel .
      BIND(CONCAT("/variants/page/", REPLACE(STR(?variant__id), "^.*\\\\/(.+)", "$1")) AS ?variant__dataProviderUrl)
      BIND("green" AS ?markerColor)
      BIND("1" AS ?instanceCount)
      
    
  } 

`

export const knowledgeGraphMetadataQuery = `
  SELECT ?id ?label
  WHERE {
    GRAPH <http://data.stadnamn.uib.no/stedsnavn/nsm>
    {
    ?id rdfs:label ?label.
    ?id rdf:type dcat:Dataset .
    }
  }
`