export const namedPlacesProperties = `
  {
    ?id a bsn:Source .
    ?id rdfs:label ?prefLabel__id .
    BIND(?prefLabel__id AS ?prefLabel__prefLabel)
    BIND(?id as ?uri__id)
  
  }
  UNION {
    ?id bsn:sourceMunicipality ?municipality__id .
     ?municipality__id  a   bsn:Municipality ;
                      rdfs:label ?municipality__prefLabel .
    
    BIND(CONCAT("/municipalities/page/", REPLACE(STR(?county__id), "^(.*)(/|#)([^#/]*)$", "$3"))  AS ?county__dataProviderUrl)
  }
  UNION {
    ?id bsn:sourceMunicipality/bsn:subunitOf ?county__id .
     ?county__id  a  rdfs:label ?municipality__prefLabel .
    
    BIND(CONCAT("/municipalities/page/", REPLACE(STR(?county__id), "^(.*)(/|#)([^#/]*)$", "$3"))  AS ?county__dataProviderUrl)
  }
`
