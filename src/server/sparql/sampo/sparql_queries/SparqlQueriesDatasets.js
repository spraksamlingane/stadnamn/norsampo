export const datasetProperties = `
  {
    ?id skos:notation ?notation .
    ?id dct:title ?prefLabel__id .
    BIND(?prefLabel__id AS ?prefLabel__prefLabel)
    BIND(?id as ?uri__id)
    BIND(?id as ?uri__prefLabel)
    BIND(?id as ?uri__dataProviderUrl)
    ?id dct:description ?description .
}

UNION {
  
  GRAPH ?id {
    ?county__id a bsn:County ;
                rdfs:label ?county__prefLabel .

    BIND(CONCAT("/counties/page/", REPLACE(STR(?county__id), "^(.*)(/|#)([^#/]*)$", "$3")) AS ?county__dataProviderUrl)
  }

}

`
