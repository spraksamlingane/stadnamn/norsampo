export const federatedSearchSparqlQueries = {
  rygh: {
    geoQuery: `
    { ?location geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) ;
      wgs84:lat ?lat;
      wgs84:long ?long . 
      ?id ecrm:P87_is_identified_by ?location . }
    `,
    textQuery: `(?prefLabel ?score) <tag:stardog:api:property:textMatch> <QUERYTERM> .`,
    resultQuery: `
    PREFIX  wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX  dct:  <http://purl.org/dc/terms/>
    PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX  skosxl: <http://www.w3.org/2008/05/skos-xl#>
    PREFIX  ecrm: <http://erlangen-crm.org/current/>
    PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
    PREFIX  skos: <http://www.w3.org/2008/05/skos#>
    PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT distinct ?id (GROUP_CONCAT (DISTINCT ?typeLabelX; separator = '/') AS ?typeLabel)
    ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?markerColor ?type_uri ?lat ?long ?positioningAccuracy ?collectionYear ?admID
    WHERE
      { GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
          { { SELECT  ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?typeLabelX
              WHERE
                { SERVICE <db://vocab>
                    { GRAPH <http://data.stadnamn.uib.no/skos/navneliste>
                        { ?type_uri  dct:identifier  ?identifier
                          VALUES ?identifier { "bruk" "gard" "navnegard" "gammelBosettingsplass" "kommune" "sokn" }
                          ?vokab_fylke  dct:identifier  "fylke" .
                          ?vokab_kommune dct:identifier  "kommune" .
                          ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabelX .
                        }
                    }
                }
            }
            GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
              { { SELECT  ?id ?prefLabel
                  WHERE
                    { <QUERY>
                      ?id  a ecrm:E53_Place;
                           rdfs:label  ?prefLabel .
                    }
                }
                ?dataset skos:notation ?datasetCode .
                ?id ecrm:P2_has_type  ?type_uri .
                ?id ecrm:P70I_is_documented_in ?nbsource .
                ?id (ecrm:P89_falls_within)* ?kommune_uri .
                ?kommune_uri  ecrm:P2_has_type  ?vokab_kommune ;
                          rdfs:label        ?kommuneLabel .
                ?kommune_uri ecrm:P89_falls_within ?fylke_uri .
                
                ?fylke_uri  ecrm:P2_has_type  ?vokab_fylke ;
                          rdfs:label        ?fylkeLabel .
                BIND("violet" AS ?markerColor)
                ?id dct:date ?collectionYear .
                OPTIONAL { ?id dct:identifier ?admID }
                BIND(CONCAT(STR(?nbsource), '&searchText=', ?prefLabel) as ?source) .  
                OPTIONAL
                  { { SELECT  ?geoCoordinatesConcept2 ?positioningAccuracy ?id ?lat ?long
                      WHERE
                        { SERVICE <db://vocab>
                            { GRAPH <http://data.stadnamn.uib.no/skos/koordinattype>
                                { 
                                  ?geoCoordinatesConcept2
                                            skos:prefLabel  ?positioningAccuracy
                                }
                            }
                          { SELECT  ?id ?geoCoordinatesConcept2 ?lat ?long
                            WHERE
                              { GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
                                  { <QUERY>
                                    ?id  rdfs:label ?prefLabel ;
                                         ecrm:P87_is_identified_by/wgs84:lat   ?lat ;
                                         ecrm:P87_is_identified_by/wgs84:long  ?long ;
                                         ecrm:P87_is_identified_by/ecrm:P2_has_type ?geoCoordinatesConcept2 }
                              }
                          }
                        }
                    }
                  }
              }
          }
      }
    GROUP BY ?id ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?markerColor ?type_uri ?lat ?long ?manifest ?score ?positioningAccuracy ?collectionYear ?admID
  
         ` 
  
  },
  hord: {
    geoQuery: `
    { ?location geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) ;
      wgs:lat ?lat;
      wgs:long ?long . 
      ?id dct:hasPart ?location;
          dct:hasPart ?row . }
    `,
    textQuery: `
    
      service fts:textMatch {
      [] fts:query <QUERYTERM>;
        fts:score ?score ;
        fts:result ?label ;
        fts:highlight ?highlight
      }
      ?row hord:namn|hord:alternativForm|hord:normertForm ?label .
     `,
    resultQuery: `
    PREFIX dcat: <http://www.w3.org/ns/dcat#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX dct: <http://purl.org/dc/terms/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX hord: <https://data.spraksamlingane.no/stadnamn/archive/hord/>
    PREFIX  text: <http://jena.apache.org/text#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX spr: <https://data.spraksamlingane.no/id/>
    PREFIX dcmitype: <http://purl.org/dc/dcmitype/>
    PREFIX fts: <tag:stardog:api:search:>
    PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

    SELECT DISTINCT ?score ?id (GROUP_CONCAT(?highlight;separator="|") as ?concatLabels) ?prefLabel ?cadastralUnit ?propertyUnit ?municipalityLabel ?countyLabel ?collection ?collectionID ?source ?dataset ?collectionYear ?datasetCode ?soundManifest ?audioFileID ?location ?lat ?long
       FROM  <https://data.spraksamlingane.no/stadnamn/archive/hord> {
        
       <QUERY>

        
          ?dataset rdf:type dcat:Dataset;
            skos:notation ?datasetCode .

          OPTIONAL { ?row hord:normertForm ?prefLabel .}

          ?id dct:hasPart ?row .

          OPTIONAL {
            ?id dct:hasPart ?location .

            ?location wgs:lat ?lat;
                      wgs:long ?long .
          }

          OPTIONAL {
            ?row hord:oppskrivingsTid ?collectionYear .
          }

          OPTIONAL {
            ?id dct:hasPart ?source .
            ?source a hord:VoiceRecording;
                    dct:identifier ?audioFileID ;
                    dct:isReferencedBy ?soundManifestURI .
            ?soundManifestURI dct:identifier ?soundManifest .
            ?soundManifestURI dct:isPartOf ?collectionURI .
            ?collectionURI rdfs:label ?collection .
            ?collectionURI dct:identifier ?collectionID .
          
        }
        
      OPTIONAL { ?row hord:kommuneNamn ?municipalityLabel .}
      OPTIONAL { ?row hord:fylkesNamn ?countyLabel .}
      OPTIONAL {
        ?row hord:bruka/hord:bruk ?bruk
        OPTIONAL { ?bruk hord:gardsNr ?cadastralUnit . }
        OPTIONAL { ?bruk hord:bruksNr ?propertyUnit . }
      }
  } GROUP BY ?score ?id ?prefLabel ?cadastralUnit ?propertyUnit ?municipalityLabel ?countyLabel ?collection ?collectionID ?source ?dataset ?collectionYear ?datasetCode ?soundManifest ?audioFilePath ?audioFileID ?location ?lat ?long
    
        `
  },
  nsnb:  {
    geoQuery: `
    { ?coordinates geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) .
      ?place crm:P189i_is_approximated_by ?coordinates . }
    `,
    textQuery: `service fts:textMatch {
      [] fts:query <QUERYTERM> ;
        fts:score ?score ;
        fts:result ?label ;
        fts:highlight ?highlight
    }`,
    resultQuery: `
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX fts: <tag:stardog:api:search:>
    PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
    SELECT ?score ?id ?concatLabel ?countyLabel ?prefLabel ?municipalityLabel ?long ?lat WHERE { GRAPH <https://data.spraksamlingane.no/stadnamn/nos-hs/nsdb> {

        <QUERY>

        

        ?place_name rdfs:label ?label ;
                a crm:E33_E41_Linguistic_Appellation .
        
        BIND(?label as ?prefLabel) .
        BIND(?highlight as ?concatLabel) .
        BIND(?place_name as ?id) .
                
        ?place_name crm:P1i_identifies ?place .
        ?place crm:P189i_is_approximated_by ?municipality .

              ?municipality rdfs:label ?municipalityLabel .
                
                OPTIONAL {
                    ?place crm:P2_has_type ?type .
                }

        ?municipality crm:P89_falls_within ?county .
        ?county rdfs:label ?countyLabel .
        OPTIONAL {
            ?place crm:P189i_is_approximated_by ?coordinates .
            ?coordinates crm:P168_place_is_defined_by ?point;
                        crm:P2_has_type ?coordinate_type .
                    bind( replace( str(?point), "^[^0-9\\\\.]*([0-9\\\\.]+) .*$", "$1" ) as ?long )
                    bind( replace( str(?point), "^.* ([0-9\\\\.]+)[^0-9\\\\.]*$", "$1" ) as ?lat )

        }
    }}
        `
  },
  bsn: {
    textQuery: `service fts:textMatch {
      [] fts:query <QUERYTERM> ;
        fts:score ?score ;
        fts:result ?label ;
        fts:highlight ?highlight
    }`,
    resultQuery: `
    PREFIX dcat: <http://www.w3.org/ns/dcat#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX dct: <http://purl.org/dc/terms/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX bsn: <https://data.spraksamlingane.no/stadnamn/archive/bsn/>
    PREFIX  text: <http://jena.apache.org/text#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX spr: <https://data.spraksamlingane.no/id/>
    PREFIX dcmitype: <http://purl.org/dc/dcmitype/>
    PREFIX fts: <tag:stardog:api:search:>
    
    SELECT DISTINCT ?score ?id (GROUP_CONCAT(?highlight;separator="|") as ?concatLabels) ?prefLabel ?cadastralUnit ?propertyUnit ?municipalityLabel ?countyLabel ?collection ?collectionID ?source ?rawDataURI ?dataset ?collectionYear ?datasetCode ?imageManifest
      FROM  <https://data.spraksamlingane.no/stadnamn/archive/bsn> {
      {
        <QUERY>


        ?id rdfs:label|bsn:data/bsn:oppslag/bsn:oppslord|bsn:data/bsn:gmlsform/bsn:navnform|bsn:data/bsn:parform/bsn:pf_navn|bsn:data/bsn:underbruk/bsn:navn ?label .

      }
      

      {

        ?dataset rdf:type dcat:Dataset;
                  skos:notation ?datasetCode .

        ?id rdfs:label ?prefLabel .

        OPTIONAL {
          ?id bsn:data ?rawDataURI .
        }
        

        OPTIONAL { ?id bsn:gnr ?cadastralUnit .}
        OPTIONAL { ?id bsn:bnr ?propertyUnit .}
        OPTIONAL { ?id bsn:municipality ?municipalityLabel .}
        OPTIONAL { ?id bsn:county ?countyLabel .}

        OPTIONAL {
          ?id dct:source ?source .
          ?source dct:isReferencedBy ?imageManifestURI ;
                  dct:date ?collectionYear .
          ?imageManifestURI dct:identifier ?imageManifest .
          ?imageManifestURI dct:isPartOf ?collectionURI .
          ?collectionURI rdfs:label ?collection .
          ?collectionURI dct:identifier ?collectionID .

        }

        OPTIONAL {
          FILTER (!bound(?imageManifestURI))
          ?id bsn:data ?data .
          OPTIONAL { ?data bsn:oppskr/bsn:år ?collectionYear . }
          OPTIONAL { ?data bsn:kjelde ?collection . }
          OPTIONAL { ?data bsn:sted/bsn:fylke ?countyLabel . }
          OPTIONAL { ?data bsn:sted/bsn:herred ?municipalityLabel . }
        }
      }

    
} GROUP BY ?score ?id ?prefLabel ?cadastralUnit ?propertyUnit ?municipalityLabel ?countyLabel ?collection ?collectionID ?source ?rawDataURI ?dataset ?collectionYear ?datasetCode ?imageManifest
    
        `
  },
  tgn: {
    resultQuery: `
        SELECT ?id (COALESCE(?labelEn,?labelGVP) AS ?prefLabel) ?typeLabel
          ?fylkeLabel ?datasetCode ?seeAlso ?lat ?long ?markerColor
        WHERE {
            ?id luc:term "<QUERYTERM>" ;
            skos:inScheme tgn: ;
            gvp:placeTypePreferred [
              gvp:prefLabelGVP [
                xl:literalForm ?typeLabel;
                dct:language gvp_lang:en
              ]
            ];
            gvp:broaderPreferred/xl:prefLabel/xl:literalForm ?fylkeLabel .
          OPTIONAL {
            ?id xl:prefLabel [
              xl:literalForm ?labelEn ;
              dct:language gvp_lang:en
            ]
          }
          OPTIONAL {
            ?id gvp:prefLabelGVP [xl:literalForm ?labelGVP]
          }
          OPTIONAL {
            ?id foaf:focus ?place .
            ?place wgs:lat ?lat ;
                   wgs:long ?long .
          }
          FILTER EXISTS {
            ?id xl:prefLabel/gvp:term+?term .
            FILTER (LCASE(STR(?term))="<QUERYTERM>")
          }
          BIND("TGN" AS ?datasetCode)
          BIND("orange" AS ?markerColor)
        }
        `
  },
}
