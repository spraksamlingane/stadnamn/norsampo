export const prefixes = `
PREFIX  vokab: <http://vokab.toponymi.spraksamlingane.no/id/>
PREFIX  vclass: <http://vokab.toponymi.spraksamlingane.no/class/>
PREFIX  vprop: <http://vokab.toponymi.spraksamlingane.no/prop/>
PREFIX  wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX  dct:  <http://purl.org/dc/terms/>
PREFIX  dcat:  <http://www.w3.org/ns/dcat#>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  skosxl: <http://www.w3.org/2008/05/skos-xl#>
PREFIX  ecrm: <http://erlangen-crm.org/current/>
PREFIX  crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  text: <http://jena.apache.org/text#>
PREFIX  spatial: <http://jena.apache.org/spatial#>
PREFIX sna: <http://data.spraksamlingane.no/stadnamn/archive/id/>
PREFIX afn: <http://jena.hpl.hp.com/ARQ/function#>
PREFIX bsn: <https://data.spraksamlingane.no/stadnamn/archive/bsn/>
PREFIX hord: <https://data.spraksamlingane.no/stadnamn/archive/hord/>
PREFIX sprak: <http://data.spraksamlingane.no/id/>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX dcmitype: <http://purl.org/dc/dcmitype/>

PREFIX crm-org: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX sch: <http://schema.org/>
PREFIX frbroo: <http://erlangen-crm.org/efrbroo/>
PREFIX mmm-schema: <http://ldf.fi/schema/mmm/>
PREFIX gvp: <http://vocab.getty.edu/ontology#>
PREFIX sd: <http://www.w3.org/ns/sparql-service-description#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
PREFIX h-schema: <http://ldf.fi/schema/hellerau/> 
PREFIX gn: <http://www.geonames.org/ontology#>
PREFIX semparls: <http://ldf.fi/schema/semparl/>

`
