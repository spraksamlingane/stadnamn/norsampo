export const municipalityProperties = `
  {
    ?id a bsn:Municipality .
    ?id rdfs:label ?prefLabel__id .
    BIND(?prefLabel__id AS ?prefLabel__prefLabel)
    BIND(?id as ?uri__id)
    BIND(?id as ?uri__dataProviderUrl)
    BIND(?id as ?uri__prefLabel)
  
  }
  UNION {
    ?id bsn:subunitOf ?county__id .
     ?county__id  a   bsn:County ;
                      rdfs:label ?county__prefLabel .
    
    BIND(CONCAT("/counties/page/", REPLACE(STR(?county__id), "^(.*)(/|#)([^#/]*)$", "$3"))  AS ?county__dataProviderUrl)
  }
  UNION {
    ?source__id bsn:sourceMunicipality ?id .
    ?stnavn a bsn:Stnavn ;
            dct:source ?source__id ;
            rdfs:label ?source__prefLabel .


    BIND(CONCAT("/counties/source/", REPLACE(STR(?county__id), "^(.*)(/|#)([^#/]*)$", "$3"))  AS ?county__dataProviderUrl)
    

  }
`
