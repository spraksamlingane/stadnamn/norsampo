const perspectiveID = 'hordanamn'

export const rowPropertiesInstancePage = `   
?id dct:hasPart ?row .

{
  ?row hord:namn ?namn .
}
UNION
{
  ?row hord:normertForm ?normertForm .
  BIND(?normertForm AS ?prefLabel)
}
UNION
{
  ?row hord:alternativForm ?alternativForm .
}
UNION {
    ?row hord:kommuneNamn ?kommuneNamn .
}
UNION {
  ?row hord:uttale ?uttale .
}
UNION {
  OPTIONAL { ?row hord:kommuneNr ?knr . }
  OPTIONAL { ?row hord:bruka/hord:bruk/hord:gardsNr ?gnr . }
  OPTIONAL { ?row hord:bruka/hord:bruk/hord:bruksNr ?bnr . }
  BIND(CONCAT(COALESCE(?knr, ""), IF(BOUND(?knr),"-",""), COALESCE(?gnr, "-"), IF(BOUND(?bnr),"/",""), COALESCE(?bnr, "")) AS ?admID)
}
UNION {
  ?row hord:fylkesNamn ?fylkesNamn .
}
UNION {
  ?row hord:merknader ?merknader .
}
UNION {
  ?row hord:oppskrivingsTid ?oppskrivingsTid .
}
UNION {
  ?row hord:kartBladNamn ?kartBladNamn .
}
UNION {
  ?row hord:kartRute ?kartRute .
}
UNION {
  ?row hord:arkivTilvising ?arkivTilvising .
}
UNION {
  ?row hord:idNr ?idNr .
}
UNION {
  ?row hord:objektNr ?objektNr .
}
UNION {
  BIND("HORD" AS ?audio__datasetCode)
  ?id dct:hasPart ?audio__id .
  ?audio__id dct:type dcmitype:Sound ;
          dct:identifier ?audio__audioFilename .
}

`

export const rowPropertiesFacetResults =
  `
  ?details__id dct:hasPart ?id .
  ?id hord:namn|hord:alternativForm|hord:normertForm ?literal .
  BIND(?literal as ?prefLabel__id)
  BIND(?literal as ?prefLabel__prefLabel)

  OPTIONAL {
    BIND("HORD" AS ?details__datasetCode)
    ?details__id dct:hasPart ?source .
    ?source a hord:VoiceRecording;
            dct:identifier ?details__audioFilename .
    }
  
  
  BIND(CONCAT("/hordanamn/page/", REPLACE(STR(?details__id), "^.*\/(.+)", "$1")) AS ?details__dataProviderUrl)
  {
      ?id hord:kommuneNamn ?kommuneNamn .
  }
  UNION {
    ?id hord:merknader ?merknader .
  }
  UNION {
    ?id hord:oppskrivingsTid ?oppskrivingsTid .
  }

`



export const hordClusterMap = `
  SELECT DISTINCT ?id ?prefLabel ?lat ?long
  WHERE { GRAPH <https://data.spraksamlingane.no/stadnamn/archive/hord> {
    <FILTER>
    
    ?row a hord:Row ;
         ^dct:hasPart ?id .
    
    OPTIONAL {
      ?row hord:normertForm ?prefLabel .
    }
          

    ?id dct:hasPart ?coordinates .
    ?coordinates wgs84:lat ?lat ;
                 wgs84:long ?long .
    
  }
}
`

export const hordPropertiesInfoWindow = `
    ?id dct:hasPart ?row .
    ?row a hord:Row .
    OPTIONAL { 
      ?row hord:normertForm ?normertForm .
    }
    OPTIONAL {
      ?row hord:namn ?namn .      
    }
    OPTIONAL {
      ?row hord:alternativForm ?alternativForm .
    }
    
    OPTIONAL {
      ?id dct:hasPart ?source .
      ?source a hord:VoiceRecording;
              dct:identifier ?audioFileID .
      }

    OPTIONAL {
        ?row hord:kommuneNamn ?kommuneNamn .
    }

    OPTIONAL { ?row hord:bruka/hord:bruk/hord:gardsNr ?gardsNr . }
    OPTIONAL { ?row hord:bruka/hord:bruk/hord:bruksNr ?bruksNr . }

    OPTIONAL {
      ?row hord:fylkesNamn ?fylkesNamn .
    }

    OPTIONAL {
      ?row hord:merknader ?merknader .
    }

    OPTIONAL {
      ?row hord:uttale ?uttale .
    }
    
    BIND("HORD" AS ?datasetCode)
    BIND(CONCAT("/hordanamn/page/", REPLACE(STR(?id), "^.*\\\\/(.+)", "$1")) AS ?infoPage)
`
