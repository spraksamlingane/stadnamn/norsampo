export const variantProperties = `
      {
      ?snid__id ecrm:P139_has_alternative_form ?id .             
      ?snid__id rdfs:label ?snidLabel ;                   
                  dct:identifier ?snid__prefLabel .

      BIND(CONCAT("/placenames/page/", REPLACE(STR(?snid__id), "^.*\\\\/(.+)", "$1")) AS ?snid__dataProviderUrl)       
      ?id rdfs:label ?prefLabel__id .              
      BIND(?prefLabel__id AS ?prefLabel__prefLabel)       
      BIND(?id as ?uri__id)       
      BIND(?id as ?uri__dataProviderUrl)       
      BIND(?id as ?uri__prefLabel)   

      }
      UNION
      {
            
      GRAPH ?g {
            ?place ecrm:P140i_was_attributed_by/ecrm:P141_assigned ?id .
            ?g dct:title ?dataset__prefLabel .
            BIND(?g AS ?dataset__id)
      }
}
          


       
      

`
